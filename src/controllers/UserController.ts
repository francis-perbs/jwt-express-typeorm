import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { validate } from "class-validator";

import { User } from "../entity/User";

class UserController {
    static listAll = async (req: Request, res: Response) => {
        // get users from database
        const userRepository = getRepository(User);
        const users = await userRepository.find({
            select: ["id", "username", "role"] // we dont want to send the passwords on response
        });

        // send the users object
        res.send(users);
    };

    static getOneById = async (req: Request, res: Response) => {
        // get the id from the url
        const id: number = req.params.id;

        //get the user from the database
        const userRepository = getRepository(User);
        try {
            const user = await userRepository.findOneOrFail(id, {
                select: ["id", "username", "role"] // we dont want to send the password on response
            });
        } catch (error) {
            res.status(404).send();
        }
    };

    static newUser = async (req: Request, res: Response) => {
        // get parameters from the body
        let { username, password, role } = req.body
        let user = new User();
        user.username = username;
        user.password = password;
        user.role = role;

        // validate if the parameters are ok
        const errors = await validate(user);
        if (errors.length > 0) {
            res.status(400).send(errors);
            return;
        }

        // hash the password, to securely store on db
        user.hashPassword();

        // try to save. if failed, the username is already in use
        const userRepository = getRepository(User);
        try {
            await userRepository.save(user);
        } catch (e) {
            res.status(409).send("username already in use");
            return;
        }

        // if all ok, send 201 response
        res.status(201).send("User created");
    };

    static editUser = async (req: Request, res: Response) => {
        // get the id from the url
        const id = req.params.id;

        // get values from the body
        const { username, role } = req.body;

        //try to find user on database
        const userRepository = getRepository(User);
        let user;
        try {
            user = await userRepository.findOneOrFail(id);
        } catch (error) {
            // if not found, send a 404 response
            res.status(404).send("User not found");
            return;
        }

        // validate the new values on model
        user.username = username;
        user.role = role;
        const errors = await validate(user);
        if (errors.length > 0) {
            res.status(400).send(errors);
            return;
        }

        //try to save, if failed, that means username is already in use
        try {
            await userRepository.save(user);
        } catch (e) {
            res.status(409).send("username already in use");
            return;
        }
        // after all send 204 response
        res.status(204).send();
    };

    static deleteUser = async (req: Request, res: Response) => {
        // get the id from the url
        const id = req.params.id;

        const userRepository = getRepository(User);
        let user: User;
        try {
            user = await userRepository.findOneOrFail(id);
        } catch (error) {
            res.status(404).send("User not found");
            return;
        }
        userRepository.delete(id);

        // successfully deleted, send 204 response
        res.status(204).send();
    };
};
export default UserController;
